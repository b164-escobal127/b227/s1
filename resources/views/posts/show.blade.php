@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <h2 class="card-title">{{$post->title}}</h2>
        <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
        <p class="card-subtitle text-muted">Likes: {{count($post->likes)}}</p>
        <p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
        <p class="card-text">{{$post->content}}</p>
        @if(Auth::id() != $post->user_id)
        <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
            @method('PUT')
            @csrf
            @if ($post->likes->contains('user_id', Auth::id()))
            <button type="submit" class="btn btn-danger">Unlike</button>
            @else
            <button type="submit" class="btn btn-success">Like</button>		
            @endif
        </form>      
        @endif

        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
            Comment
        </button>
        

        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Comment on a post</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="modal-body" method="POST" action="/posts/{{$post->id}}/comment">
                        @csrf
                        <div class="mb-3">                            
                            <textarea class="form-control" id="content" name="content" rows="3"  placeholder="Write your comment"></textarea>
                        </div>
                        <div>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Comment</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> 

        
        @if (count($post->comments) > 0)
        @foreach ($post->comments as $comment)
        <div class=" text-center my-2 border rounded">
            <div class="">
                <h6 class=" mb-1"><a href="/posts/{{$post->id}}">{{$comment->user->name}}</a></h6>                
                <p class=" mb-1 text-muted">{{$comment->content}}</p>
             </div>
        </div>
        
        @endforeach
        @endif
        <div class="mt-3">
            <a href="/posts" class="card-link">View all posts</a>
        </div>
    </div>
</div>
@endsection

